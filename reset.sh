#!/bin/bash

cat > ./07-dom/main.go <<- EOM
package main
// dom
import (
  // this package allows the WebAssembly program
  // to access the host environment (the browser)
  "syscall/js"
)

func main() {

  message := "👋 Hello World from TinyGo 🌍"

  // 1- reference to the DOM
  
  // 2- create H2 element

  // 3- create H3 element

  // 4- add elements to body

}

EOM


cat > ./08-return-json/main.go <<- EOM
package main
// json
import (
  "syscall/js"
)

// 1- Create Hello function

func main() {
  // 4- "export" Hello function

  <-make(chan bool)
}

EOM

cat > ./08-return-json/index.html <<- EOM
<html>
  <head>
    <meta charset="utf-8"/>
    <link rel="stylesheet" href="my.css">
    <script src="wasm_exec.js"></script>
  </head>
  <body>
    <h1>WASM Experiments</h1>
    <h2>This is a subtitle</h2>
    <!-- json -->
    <script> 

      function loadWasm(path) {
        const go = new Go()

        // Hack for use TinyGo
        // https://github.com/tinygo-org/tinygo/issues/1140
        go.importObject.gojs["syscall/js.finalizeRef"] = _ => 0 // 😉

        return new Promise((resolve, reject) => {
          WebAssembly.instantiateStreaming(fetch(path), go.importObject)
          .then(result => {
            go.run(result.instance)
            resolve(result.instance)
          })
          .catch(error => {
            reject(error)
          })
        })
      }

      loadWasm("main.wasm").then(wasm => {
        console.log("😃 main.wasm is loaded")

        // 5- Call Wasm Hello function


        // 6- Use the JSON data


      }).catch(error => {
        console.log("😡 ouch", error)
      }) 

    </script>
  </body>
</html>

EOM


cat > ./10-call-a-js-function/main.go <<- EOM
package main
// func
import (
  "fmt"
  "syscall/js"
)

func main() {
  // 1- Call the JavaScript function

  // 2- Get the value of the JavaScript variable (message)

  // 3- Change the message

  // 4- Get Human information

  // 5- Change Human properties

  <-make(chan bool)
}

EOM

cat > ./11-call-a-promise/main.go <<- EOM
package main
// prom
import (
    "fmt"
    "syscall/js"
)

func main() {

    // 1- if everything goes well
    
    // 2- catch the error
    
    // 3- it's a success
    
    // 4- it's a fail

    <-make(chan bool)
}

EOM