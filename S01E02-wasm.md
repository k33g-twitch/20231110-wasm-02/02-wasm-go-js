---
marp: true
---
# S01E02

## WebAssembly (Wasm), GoLang, TinyGo and JavaScript

> - Repo: https://gitlab.com/k33g-twitch/20231110-wasm-02/02-wasm-go-js
> - 👀 `README.md` => Open it with Docker Development Environment

---
# Agenda

## Demos
- Document Object Model
- return JSON
- JSON as argument
- Call a JavaScript function
- Call a Promise

## References
- Reading and Writing Graphics
- Reading and Writing Audio

---
# Reading and Writing Graphics

![width:400px](imgs/pixels.png "pixels")
> https://riptutorial.com/html5-canvas/example/19790/introduction-to--context-getimagedata-
---
# Reading and Writing Graphics

- [Reading and Writing Graphics (with wasm)](https://wasmbyexample.dev/examples/reading-and-writing-graphics/reading-and-writing-graphics.go.en-us.html)
- HTML/JavaScript:
  - [API/ImageData](https://developer.mozilla.org/en-US/docs/Web/API/ImageData)
  - [Pixel manipulation with canvas](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial/Pixel_manipulation_with_canvas)
  - [Example: Canvas ImageData data Property](https://www.w3schools.com/jsref/canvas_imagedata_data.asp)
  - [Example: HTML5 Canvas Image Data](http://www.java2s.com/Tutorials/HTML_CSS/HTML5_Canvas/0620__HTML5_Canvas_Image_Data.htm)

---
# Reading and Writing Audio

- [Reading and Writing Audio (with wasm)](https://wasmbyexample.dev/examples/reading-and-writing-audio/reading-and-writing-audio.go.en-us.html)

---
# Crazy: Container to Wasm

>[https://x.com/yrougy/status/1722360757856203224?s=20](https://x.com/yrougy/status/1722360757856203224?s=20)
![width:400px](imgs/yves.png "pixels")

- [container2wasm](https://github.com/ktock/container2wasm)
- [Debian demo](https://ktock.github.io/container2wasm-demo/amd64-debian-wasi.html)

---
# Wanix

- https://x.com/progrium/status/1722689504299905455?s=20

---
# Next time(s):

- S01E03: Wasm, Rust, JavaScript
- S01E04: ??? (.Net 🤔)

